import { connect } from 'react-redux';
import { getDataFromJSONPlaceholder } from '../actions/general';
import Button from '../components/Button';

const mapStateToProps = () => {
    return { };
};

const mapDispatchToProps = (dispatch) => ({
    getDataFromJSONPlaceholder: () => {
        dispatch(getDataFromJSONPlaceholder());
    }
});

const ButtonContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Button);

export default ButtonContainer;

