import React, { Component } from 'react';

class Posts extends Component {
    render() {
        const { posts } = this.props;
        const postsContent = [];
        for (const { title, body: content } of posts) {
            postsContent.push(
                <div>
                    <h1>{title}</h1>
                    <p>{content}</p>
                </div>
            )
        }
        return (
            <div className="Posts">
                {postsContent}
            </div>
        );
    }
}

export default Posts;
