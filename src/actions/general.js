import fetch from 'cross-fetch';

export const getDataFromJSONPlaceholder = () => ({
    type: 'GET_DATA_FROM_JSON_PLACEHOLDER',
    promise:
        fetch('https://jsonplaceholder.typicode.com/posts')
        .then((response) => response.json()),
});
