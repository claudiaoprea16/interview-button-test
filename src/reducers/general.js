import { Record } from 'immutable';
import { handle } from 'redux-pack';

const GeneralState = Record({
    posts: [],
});

export default (state = new GeneralState(), action) => {
    switch (action.type) {
        case 'GET_DATA_FROM_JSON_PLACEHOLDER':
            return handle(state, action, {
                success: prevState => {
                    return prevState.set('posts', action.payload);
                },
                // failure: prevState => { }
            });
        default:
            return state;
    }
}